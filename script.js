function clockRunner() {
  var d = new Date();
  var Hour = d.getHours();
  var minute = d.getMinutes();
  var second = d.getSeconds();

  const body = document.body;
  const watchColor = document.getElementById("myClockDisplay");

  var p = "AM";

  if (Hour > 12) {
    Hour -= 12;
    p = "PM";

    body.style.backgroundColor = "#ac880f";
    watchColor.style.color = "#fff";
    watchColor.style.backgroundColor = '#333';
  } else if (Hour > 18) {
    body.style.backgroundColor = "#050430";
    watchColor.style.color = "#fff";
    watchColor.style.backgroundColor = "#3a3e3f96";
  }

  if (Hour < 10) {
    Hour = "0" + Hour;
  }

  if (minute < 10) {
    minute = "0" + minute;
  }
  if (second < 10) {
    second = "0" + second;
  }

  var clock = Hour + ":" + minute + ":" + second + " " + p;
  document.getElementById("myClockDisplay").textContent = clock;
  if (Hour) setTimeout(clockRunner, 1000);
}
clockRunner();
